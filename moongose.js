const mongoose = require('mongoose');

main().catch(err => console.log(err));

async function main() {
  await mongoose.connect('mongodb://localhost:27017/db_latihan');
}

const userSchema = new mongoose.Schema({
    name: String,
    age: Number,
    status: {
        type: String,
        enum: ['active', 'non active'],
        default: 'non active'
    }
  });

const User = mongoose.model('User', userSchema);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', async  () => {

    //GET
    const users = await User.find()

    console.log(users)

  

    // console.log(newUser)
    // POST
    // const newUser = await User.create({
    //     name: 'faisal depay',
    //     age: 24,
    //     status: 'active'
    // })
    // const newUser = new User()
    // newUser.name = 'iyan';
    // newUser.age = 14;
    // newUser.status = 'non active';
    // const insert = await newUser.save()
    // console.log(insert)

    //Update
    // const UpdateUser = await User.updateOne({ _id: "62b1a356a38c3e7d8af0f93d"}, {name: "Iyan Jelek"})
    // const UpdateUser = await User.findById("62b1a356a38c3e7d8af0f93d")
    // UpdateUser.name = "iyan"
    // const update = await UpdateUser.save()
    // console.log(UpdateUser)

    //Delete
    // const DeleteUser = await User.deleteOne({_id:"62b1a356a38c3e7d8af0f93d"})
    // console.log(DeleteUser)
})