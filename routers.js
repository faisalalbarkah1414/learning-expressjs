const express = require('express')
const router = express.Router()
const {ObjectId} = require('mongodb')
const connection = require('./connection')

router.get('/', (req, res) => {
    res.send('Hello World!')
})
  
router.get('/users', async (req, res) => {
    try{
        console.log('db',connection.isConnected())
        if(connection.isConnected())
        {
            const db = connection.db('db_latihan')
            const users = await db.collection('users').find().toArray()
            res.send({data: users})
        }else{
            res.send({message:"Koneksi Database Gagal"})
        }
    }
    catch (err){
    res.send({ message: err.message || 'internal server error' })
    }
})

router.post('/users', async (req, res) => {
    try{
        if(connection.isConnected())
        {
            const { name, age, status } = req.body
            const db = connection.db('db_latihan')
            const users = await db.collection('users').insertOne({
                name, age, status
            })
            if(users.insertedCount === 1)
            {
                res.send({message: "Berhasil ditambahkan"})
            }else{
                res.send({message: "Gagal Menambahkan data"})
            }
            
        }else{
            res.send({message:"Koneksi Database Gagal"})
        }
    }
    catch (err){
    res.send({ message: err.message || 'internal server error' })
    }
})

router.put('/users/:id', async (req, res) => {
    try{
        if(connection.isConnected())
        {
            const {id} = req.params
            const { name, age, status } = req.body
            const db = connection.db('db_latihan')
            const users = await db.collection('users').updateOne({_id: ObjectId(id)},{
               $set: {
                name, age, status
               }
            })
            console.log('users',users)
            if(users.modifiedCount === 1)
            {
                res.send({message: "Berhasil Diubah"})
            }else{
                res.send({message: "Gagal Mengubah data"})
            }
            
        }else{
            res.send({message:"Koneksi Database Gagal"})
        }
    }
    catch (err){
    res.send({ message: err.message || 'internal server error' })
    }
})

router.delete('/users/:id', async (req, res) => {
    try{
        if(connection.isConnected())
        {
            const {id} = req.params
            const db = connection.db('db_latihan')
            const users = await db.collection('users').deleteOne({_id: ObjectId(id)})
            if(users.deletedCount === 1)
            {
                res.send({message: "Berhasil Menghapus"})
            }else{
                res.send({message: "Gagal Menghapus data"})
            }
            
        }else{
            res.send({message:"Koneksi Database Gagal"})
        }
    }
    catch (err){
    res.send({ message: err.message || 'internal server error' })
    }
})
  

module.exports = router